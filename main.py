def compare(time1, time2):
    if time1[0] > time2[0]:
        return -1
    elif time1[0] == time2[0]:
        if time1[1] > time2[1]:
            return 1
        elif time1[1] == time2[1]:
            return 0
        else:
            return -1
    else:
        return -1


def to_time(string, divider):
    return tuple([int(value) for value in string.split(divider)])


if __name__ == "__main__":
    DIVIDER = '.'

    time = to_time(input("What the time is now?\n"), DIVIDER)
    message = ""

    if compare(time, (8, 0)) >= 0 > compare(time, (11, 0)):
        message = "Good Morning!"
    elif compare(time, (11, 0)) >= 0 > compare(time, (18, 0)):
        message = "Good Afternoon!" if compare(time, (12, 0)) else "Time to eat!"
    elif compare(time, (18, 0)) >= 0 > compare(time, (22, 0)):
        message = "Good Evening!"
    elif compare(time, (23, 0)) >= 0 or compare(time, (6, 0)) <= 0:
        message = "It's time for tash!"

    print(message)
